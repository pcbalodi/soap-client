package com.grailsdevtest

import groovy.json.*
import com.grailsdevtest.client.Category

import grails.converters.JSON



class CategoriesController {

  def categoriesTableStatic(){
    [categories: new CategoriesCommand().execute()]
  }

  def categoriesTableDynamic(){

  }

  def categoriesAjax(){
    List<Category> categories = new CategoriesCommand().execute()
    render (getCategoryProps(categories) as JSON)
  }

  private List getCategoryProps(List<Category>categories){
    categories.collect {
      ["id": it.id,
       "docType": it.docType,
       "docGroup": it.docGroup,
       "description": it.description]
    }
  }

}
