<html>
  <head>
    <meta name="layout" content="main"/>
    <title> Static categories </title>
  </head>
  <body>

    <div id="categories-container">
      <g:if test="${categories}">
	<div class="category header">
	  <span class="group">
	    Doc Group
	  </span>
	  <span>
	    Doc Type
	  </span>
	  <span>
	    Description
	  </span>
	</div>
	<g:each in ="${categories}">
	  <div class="category">
	    <span class="group">
	      ${it.docGroup}
	    </span>
	    <span>
	      ${it.docType}
	    </span>
	    <span>
	      ${it.description}
	    </span>
	  </div>
	</g:each>
      </g:if>
      <g:else>
	Error loading Categories
      </g:else>
    </div>

  </body>
</html>
