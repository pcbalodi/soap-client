<html>
  <head>
    <meta name="layout" content="main"/>
    <title> Dynamic categories </title>
  </head>
  <body>

    <button id="loadCategories">Refresh</button>

    <div id="categories-container">
    </div>

    <div id="categoryHtmlClone" style="display:none">
      <div class="category">
	<span class="group">
	  ##docGroup##
	</span>
	<span>
	  ##docType##
	</span>
	<span>
	  ##description##
	</span>
      </div>
    </div>

    <div id="categoryHtmlHeaderClone" style="display:none">
      <div class="category header">
	<span class="group">
	  Doc Group
	</span>
	<span>
	  Doc Type
	</span>
	<span>
	  Description
	</span>
      </div>
    </div>


    <g:link controller="categories" action="categoriesAjax" style="display:none" elementId="getCategoriesLink">
    </g:link>

  </body>
</html>
