//= require jquery

if (typeof jQuery !== 'undefined') {
  (function($) {
    $(document).ajaxStart(function(){
      $('#spinner').fadeIn();
    }).ajaxStop(function(){
      $('#spinner').fadeOut();
    });
  })(jQuery);
}

jQuery(document).ready(function(){
  bindLoadCategoryHandler();
  window.categoryHtml= $("#categoryHtmlClone").html();
  window.categoryHeaderHtml= $("#categoryHtmlHeaderClone").html();
  $("#categoryHtmlClone").remove();
});

function bindLoadCategoryHandler(){

  jQuery("#loadCategories").on("click",fetchCategories)
}

function fetchCategories(){
  $.get($("#getCategoriesLink").attr("href"),updateCategories)
}

function updateCategories(categories){
  var $container = jQuery("#categories-container");
  $container.html('');
  if(categories.length>0){
    $container.append(window.categoryHeaderHtml);
    jQuery.each(categories, function(idx, category){
      var html = window.categoryHtml;
      $container.append(html.replace("##docGroup##",category['docGroup'])
		        .replace("##docType##",category['docType'])
			.replace("##description##",category['description']))
    })
      }
  else{
    $container.text("No categories found")
  }

}
