# README #

This Project implements soap-client for the provided web service.

### Launching the soap server ###

On Linux and mac, You can launch the provided web service by running the following script from the project root - 

```
bin/launch-soap-server.sh
```

### Generating the soap-client ###
You can generate the soap-client by running the following script - 

```
bin/generate-client.sh
```

The above script will generate the required classes and place them on the correct folder. Alternatively, you can use the following command - 

```
wsimport -Xnocompile -p com.grailsdevtest.client -d src/java/ http://localhost:9090/ARIISRequestTestService?wsdl
```

### Launching Netflix Hystrix dashboard ###
Netflix Hystrix dashboard is an application for monitoring the status of circuit breaker, u can set it up with - 

```
$ git clone git@github.com:Netflix/Hystrix.git
$ cd Hystrix/hystrix-dashboard
$ ../gradlew jettyRun
> Building > :hystrix-dashboard:jettyRun > Running at http://localhost:7979/hystrix-dashboard
```

Or look at the Following github page - https://github.com/Netflix/Hystrix/wiki/Dashboard.

Once the app is running, setup monitoring with following steps - 

* go to http://localhost:7979/hystrix-dashboard
* add Following url for monitoring - 
```
http://localhost:8080/soap-client/hystrix.stream
```
* Click on ``` Add Stream ```
* Click on ``` Monitor Streams ```


### Setting up Jmeter ####

For Load testing with Jmeter, follow these steps -

* Install jmeter (brew install jmeter --with-plugins)
* Load the test plan, its at following location - 
```
jmeter/Test-plan.jmx
```
Run the tests by clicking on the Play/Start icon. You can see monitor the activity on hystrix dashboard now.