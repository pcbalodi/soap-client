
package com.grailsdevtest.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for documentCreationProcessingResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="documentCreationProcessingResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://server.ws.ariis.quadnova.com/}processingResponse">
 *       &lt;sequence>
 *         &lt;element name="creationCompleted" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="docId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="fileId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="jobId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentCreationProcessingResponse", propOrder = {
    "creationCompleted",
    "docId",
    "fileId",
    "jobId"
})
public class DocumentCreationProcessingResponse
    extends ProcessingResponse
{

    protected boolean creationCompleted;
    protected Long docId;
    protected Long fileId;
    protected Long jobId;

    /**
     * Gets the value of the creationCompleted property.
     * 
     */
    public boolean isCreationCompleted() {
        return creationCompleted;
    }

    /**
     * Sets the value of the creationCompleted property.
     * 
     */
    public void setCreationCompleted(boolean value) {
        this.creationCompleted = value;
    }

    /**
     * Gets the value of the docId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDocId() {
        return docId;
    }

    /**
     * Sets the value of the docId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDocId(Long value) {
        this.docId = value;
    }

    /**
     * Gets the value of the fileId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFileId() {
        return fileId;
    }

    /**
     * Sets the value of the fileId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFileId(Long value) {
        this.fileId = value;
    }

    /**
     * Gets the value of the jobId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getJobId() {
        return jobId;
    }

    /**
     * Sets the value of the jobId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setJobId(Long value) {
        this.jobId = value;
    }

}
