
package com.grailsdevtest.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.grailsdevtest.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _WebServiceParameterRequiredException_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "WebServiceParameterRequiredException");
    private final static QName _AddConceptOneContractIndexToDocumentResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "addConceptOneContractIndexToDocumentResponse");
    private final static QName _GetDocumentTree_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "getDocumentTree");
    private final static QName _ChangeDocumentData_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "changeDocumentData");
    private final static QName _WebServiceSecurityException_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "WebServiceSecurityException");
    private final static QName _GetAssignedDocumentsInDesktop_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "getAssignedDocumentsInDesktop");
    private final static QName _AddConceptOneAgencyIndexToDocument_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "addConceptOneAgencyIndexToDocument");
    private final static QName _AddConceptOneContractIndexToDocument_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "addConceptOneContractIndexToDocument");
    private final static QName _GetAbsoluteDocumentUrlResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "getAbsoluteDocumentUrlResponse");
    private final static QName _Category_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "category");
    private final static QName _ImportOutlookEmail_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "importOutlookEmail");
    private final static QName _GetCategoriesResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "getCategoriesResponse");
    private final static QName _ImportDocumentFromEmail_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "importDocumentFromEmail");
    private final static QName _ImportDocumentFromPdf_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "importDocumentFromPdf");
    private final static QName _ForwardEmailToOutlook_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "forwardEmailToOutlook");
    private final static QName _GetDocumentTreeResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "getDocumentTreeResponse");
    private final static QName _GetOutlookEmailsResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "getOutlookEmailsResponse");
    private final static QName _ImportOutlookEmailResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "importOutlookEmailResponse");
    private final static QName _GetAbsoluteDocumentUrl_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "getAbsoluteDocumentUrl");
    private final static QName _WebServiceProcessingException_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "WebServiceProcessingException");
    private final static QName _ChangeDocumentDataResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "changeDocumentDataResponse");
    private final static QName _AssignDocumentToUserResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "assignDocumentToUserResponse");
    private final static QName _GetCategories_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "getCategories");
    private final static QName _ImportDocumentFromEmailResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "importDocumentFromEmailResponse");
    private final static QName _ForwardEmailToOutlookResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "forwardEmailToOutlookResponse");
    private final static QName _ImportDocumentFromPdfResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "importDocumentFromPdfResponse");
    private final static QName _GetOutlookEmails_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "getOutlookEmails");
    private final static QName _AssignDocumentToUser_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "assignDocumentToUser");
    private final static QName _AddConceptOneAgencyIndexToDocumentResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "addConceptOneAgencyIndexToDocumentResponse");
    private final static QName _GetAssignedDocumentsInDesktopResponse_QNAME = new QName("http://server.ws.ariis.quadnova.com/", "getAssignedDocumentsInDesktopResponse");
    private final static QName _ImportDocumentFromPdfArg1_QNAME = new QName("", "arg1");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.grailsdevtest.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ImportOutlookEmailResponse }
     * 
     */
    public ImportOutlookEmailResponse createImportOutlookEmailResponse() {
        return new ImportOutlookEmailResponse();
    }

    /**
     * Create an instance of {@link GetAbsoluteDocumentUrl }
     * 
     */
    public GetAbsoluteDocumentUrl createGetAbsoluteDocumentUrl() {
        return new GetAbsoluteDocumentUrl();
    }

    /**
     * Create an instance of {@link ForwardEmailToOutlook }
     * 
     */
    public ForwardEmailToOutlook createForwardEmailToOutlook() {
        return new ForwardEmailToOutlook();
    }

    /**
     * Create an instance of {@link GetDocumentTreeResponse }
     * 
     */
    public GetDocumentTreeResponse createGetDocumentTreeResponse() {
        return new GetDocumentTreeResponse();
    }

    /**
     * Create an instance of {@link GetOutlookEmailsResponse }
     * 
     */
    public GetOutlookEmailsResponse createGetOutlookEmailsResponse() {
        return new GetOutlookEmailsResponse();
    }

    /**
     * Create an instance of {@link AssignDocumentToUserResponse }
     * 
     */
    public AssignDocumentToUserResponse createAssignDocumentToUserResponse() {
        return new AssignDocumentToUserResponse();
    }

    /**
     * Create an instance of {@link WebServiceProcessingException }
     * 
     */
    public WebServiceProcessingException createWebServiceProcessingException() {
        return new WebServiceProcessingException();
    }

    /**
     * Create an instance of {@link ChangeDocumentDataResponse }
     * 
     */
    public ChangeDocumentDataResponse createChangeDocumentDataResponse() {
        return new ChangeDocumentDataResponse();
    }

    /**
     * Create an instance of {@link ForwardEmailToOutlookResponse }
     * 
     */
    public ForwardEmailToOutlookResponse createForwardEmailToOutlookResponse() {
        return new ForwardEmailToOutlookResponse();
    }

    /**
     * Create an instance of {@link ImportDocumentFromPdfResponse }
     * 
     */
    public ImportDocumentFromPdfResponse createImportDocumentFromPdfResponse() {
        return new ImportDocumentFromPdfResponse();
    }

    /**
     * Create an instance of {@link GetCategories }
     * 
     */
    public GetCategories createGetCategories() {
        return new GetCategories();
    }

    /**
     * Create an instance of {@link ImportDocumentFromEmailResponse }
     * 
     */
    public ImportDocumentFromEmailResponse createImportDocumentFromEmailResponse() {
        return new ImportDocumentFromEmailResponse();
    }

    /**
     * Create an instance of {@link AddConceptOneAgencyIndexToDocumentResponse }
     * 
     */
    public AddConceptOneAgencyIndexToDocumentResponse createAddConceptOneAgencyIndexToDocumentResponse() {
        return new AddConceptOneAgencyIndexToDocumentResponse();
    }

    /**
     * Create an instance of {@link GetAssignedDocumentsInDesktopResponse }
     * 
     */
    public GetAssignedDocumentsInDesktopResponse createGetAssignedDocumentsInDesktopResponse() {
        return new GetAssignedDocumentsInDesktopResponse();
    }

    /**
     * Create an instance of {@link GetOutlookEmails }
     * 
     */
    public GetOutlookEmails createGetOutlookEmails() {
        return new GetOutlookEmails();
    }

    /**
     * Create an instance of {@link AssignDocumentToUser }
     * 
     */
    public AssignDocumentToUser createAssignDocumentToUser() {
        return new AssignDocumentToUser();
    }

    /**
     * Create an instance of {@link AddConceptOneContractIndexToDocumentResponse }
     * 
     */
    public AddConceptOneContractIndexToDocumentResponse createAddConceptOneContractIndexToDocumentResponse() {
        return new AddConceptOneContractIndexToDocumentResponse();
    }

    /**
     * Create an instance of {@link WebServiceParameterRequiredException }
     * 
     */
    public WebServiceParameterRequiredException createWebServiceParameterRequiredException() {
        return new WebServiceParameterRequiredException();
    }

    /**
     * Create an instance of {@link AddConceptOneAgencyIndexToDocument }
     * 
     */
    public AddConceptOneAgencyIndexToDocument createAddConceptOneAgencyIndexToDocument() {
        return new AddConceptOneAgencyIndexToDocument();
    }

    /**
     * Create an instance of {@link GetDocumentTree }
     * 
     */
    public GetDocumentTree createGetDocumentTree() {
        return new GetDocumentTree();
    }

    /**
     * Create an instance of {@link ChangeDocumentData }
     * 
     */
    public ChangeDocumentData createChangeDocumentData() {
        return new ChangeDocumentData();
    }

    /**
     * Create an instance of {@link WebServiceSecurityException }
     * 
     */
    public WebServiceSecurityException createWebServiceSecurityException() {
        return new WebServiceSecurityException();
    }

    /**
     * Create an instance of {@link GetAssignedDocumentsInDesktop }
     * 
     */
    public GetAssignedDocumentsInDesktop createGetAssignedDocumentsInDesktop() {
        return new GetAssignedDocumentsInDesktop();
    }

    /**
     * Create an instance of {@link GetAbsoluteDocumentUrlResponse }
     * 
     */
    public GetAbsoluteDocumentUrlResponse createGetAbsoluteDocumentUrlResponse() {
        return new GetAbsoluteDocumentUrlResponse();
    }

    /**
     * Create an instance of {@link AddConceptOneContractIndexToDocument }
     * 
     */
    public AddConceptOneContractIndexToDocument createAddConceptOneContractIndexToDocument() {
        return new AddConceptOneContractIndexToDocument();
    }

    /**
     * Create an instance of {@link GetCategoriesResponse }
     * 
     */
    public GetCategoriesResponse createGetCategoriesResponse() {
        return new GetCategoriesResponse();
    }

    /**
     * Create an instance of {@link ImportDocumentFromEmail }
     * 
     */
    public ImportDocumentFromEmail createImportDocumentFromEmail() {
        return new ImportDocumentFromEmail();
    }

    /**
     * Create an instance of {@link ImportDocumentFromPdf }
     * 
     */
    public ImportDocumentFromPdf createImportDocumentFromPdf() {
        return new ImportDocumentFromPdf();
    }

    /**
     * Create an instance of {@link Category }
     * 
     */
    public Category createCategory() {
        return new Category();
    }

    /**
     * Create an instance of {@link ImportOutlookEmail }
     * 
     */
    public ImportOutlookEmail createImportOutlookEmail() {
        return new ImportOutlookEmail();
    }

    /**
     * Create an instance of {@link ProcessingResponse }
     * 
     */
    public ProcessingResponse createProcessingResponse() {
        return new ProcessingResponse();
    }

    /**
     * Create an instance of {@link Document }
     * 
     */
    public Document createDocument() {
        return new Document();
    }

    /**
     * Create an instance of {@link ExchangeEmail }
     * 
     */
    public ExchangeEmail createExchangeEmail() {
        return new ExchangeEmail();
    }

    /**
     * Create an instance of {@link BaseObject }
     * 
     */
    public BaseObject createBaseObject() {
        return new BaseObject();
    }

    /**
     * Create an instance of {@link DocumentCreationProcessingResponse }
     * 
     */
    public DocumentCreationProcessingResponse createDocumentCreationProcessingResponse() {
        return new DocumentCreationProcessingResponse();
    }

    /**
     * Create an instance of {@link DocumentSet }
     * 
     */
    public DocumentSet createDocumentSet() {
        return new DocumentSet();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebServiceParameterRequiredException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "WebServiceParameterRequiredException")
    public JAXBElement<WebServiceParameterRequiredException> createWebServiceParameterRequiredException(WebServiceParameterRequiredException value) {
        return new JAXBElement<WebServiceParameterRequiredException>(_WebServiceParameterRequiredException_QNAME, WebServiceParameterRequiredException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddConceptOneContractIndexToDocumentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "addConceptOneContractIndexToDocumentResponse")
    public JAXBElement<AddConceptOneContractIndexToDocumentResponse> createAddConceptOneContractIndexToDocumentResponse(AddConceptOneContractIndexToDocumentResponse value) {
        return new JAXBElement<AddConceptOneContractIndexToDocumentResponse>(_AddConceptOneContractIndexToDocumentResponse_QNAME, AddConceptOneContractIndexToDocumentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDocumentTree }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "getDocumentTree")
    public JAXBElement<GetDocumentTree> createGetDocumentTree(GetDocumentTree value) {
        return new JAXBElement<GetDocumentTree>(_GetDocumentTree_QNAME, GetDocumentTree.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeDocumentData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "changeDocumentData")
    public JAXBElement<ChangeDocumentData> createChangeDocumentData(ChangeDocumentData value) {
        return new JAXBElement<ChangeDocumentData>(_ChangeDocumentData_QNAME, ChangeDocumentData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebServiceSecurityException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "WebServiceSecurityException")
    public JAXBElement<WebServiceSecurityException> createWebServiceSecurityException(WebServiceSecurityException value) {
        return new JAXBElement<WebServiceSecurityException>(_WebServiceSecurityException_QNAME, WebServiceSecurityException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAssignedDocumentsInDesktop }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "getAssignedDocumentsInDesktop")
    public JAXBElement<GetAssignedDocumentsInDesktop> createGetAssignedDocumentsInDesktop(GetAssignedDocumentsInDesktop value) {
        return new JAXBElement<GetAssignedDocumentsInDesktop>(_GetAssignedDocumentsInDesktop_QNAME, GetAssignedDocumentsInDesktop.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddConceptOneAgencyIndexToDocument }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "addConceptOneAgencyIndexToDocument")
    public JAXBElement<AddConceptOneAgencyIndexToDocument> createAddConceptOneAgencyIndexToDocument(AddConceptOneAgencyIndexToDocument value) {
        return new JAXBElement<AddConceptOneAgencyIndexToDocument>(_AddConceptOneAgencyIndexToDocument_QNAME, AddConceptOneAgencyIndexToDocument.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddConceptOneContractIndexToDocument }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "addConceptOneContractIndexToDocument")
    public JAXBElement<AddConceptOneContractIndexToDocument> createAddConceptOneContractIndexToDocument(AddConceptOneContractIndexToDocument value) {
        return new JAXBElement<AddConceptOneContractIndexToDocument>(_AddConceptOneContractIndexToDocument_QNAME, AddConceptOneContractIndexToDocument.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAbsoluteDocumentUrlResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "getAbsoluteDocumentUrlResponse")
    public JAXBElement<GetAbsoluteDocumentUrlResponse> createGetAbsoluteDocumentUrlResponse(GetAbsoluteDocumentUrlResponse value) {
        return new JAXBElement<GetAbsoluteDocumentUrlResponse>(_GetAbsoluteDocumentUrlResponse_QNAME, GetAbsoluteDocumentUrlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Category }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "category")
    public JAXBElement<Category> createCategory(Category value) {
        return new JAXBElement<Category>(_Category_QNAME, Category.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportOutlookEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "importOutlookEmail")
    public JAXBElement<ImportOutlookEmail> createImportOutlookEmail(ImportOutlookEmail value) {
        return new JAXBElement<ImportOutlookEmail>(_ImportOutlookEmail_QNAME, ImportOutlookEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCategoriesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "getCategoriesResponse")
    public JAXBElement<GetCategoriesResponse> createGetCategoriesResponse(GetCategoriesResponse value) {
        return new JAXBElement<GetCategoriesResponse>(_GetCategoriesResponse_QNAME, GetCategoriesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDocumentFromEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "importDocumentFromEmail")
    public JAXBElement<ImportDocumentFromEmail> createImportDocumentFromEmail(ImportDocumentFromEmail value) {
        return new JAXBElement<ImportDocumentFromEmail>(_ImportDocumentFromEmail_QNAME, ImportDocumentFromEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDocumentFromPdf }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "importDocumentFromPdf")
    public JAXBElement<ImportDocumentFromPdf> createImportDocumentFromPdf(ImportDocumentFromPdf value) {
        return new JAXBElement<ImportDocumentFromPdf>(_ImportDocumentFromPdf_QNAME, ImportDocumentFromPdf.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForwardEmailToOutlook }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "forwardEmailToOutlook")
    public JAXBElement<ForwardEmailToOutlook> createForwardEmailToOutlook(ForwardEmailToOutlook value) {
        return new JAXBElement<ForwardEmailToOutlook>(_ForwardEmailToOutlook_QNAME, ForwardEmailToOutlook.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDocumentTreeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "getDocumentTreeResponse")
    public JAXBElement<GetDocumentTreeResponse> createGetDocumentTreeResponse(GetDocumentTreeResponse value) {
        return new JAXBElement<GetDocumentTreeResponse>(_GetDocumentTreeResponse_QNAME, GetDocumentTreeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOutlookEmailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "getOutlookEmailsResponse")
    public JAXBElement<GetOutlookEmailsResponse> createGetOutlookEmailsResponse(GetOutlookEmailsResponse value) {
        return new JAXBElement<GetOutlookEmailsResponse>(_GetOutlookEmailsResponse_QNAME, GetOutlookEmailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportOutlookEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "importOutlookEmailResponse")
    public JAXBElement<ImportOutlookEmailResponse> createImportOutlookEmailResponse(ImportOutlookEmailResponse value) {
        return new JAXBElement<ImportOutlookEmailResponse>(_ImportOutlookEmailResponse_QNAME, ImportOutlookEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAbsoluteDocumentUrl }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "getAbsoluteDocumentUrl")
    public JAXBElement<GetAbsoluteDocumentUrl> createGetAbsoluteDocumentUrl(GetAbsoluteDocumentUrl value) {
        return new JAXBElement<GetAbsoluteDocumentUrl>(_GetAbsoluteDocumentUrl_QNAME, GetAbsoluteDocumentUrl.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebServiceProcessingException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "WebServiceProcessingException")
    public JAXBElement<WebServiceProcessingException> createWebServiceProcessingException(WebServiceProcessingException value) {
        return new JAXBElement<WebServiceProcessingException>(_WebServiceProcessingException_QNAME, WebServiceProcessingException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeDocumentDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "changeDocumentDataResponse")
    public JAXBElement<ChangeDocumentDataResponse> createChangeDocumentDataResponse(ChangeDocumentDataResponse value) {
        return new JAXBElement<ChangeDocumentDataResponse>(_ChangeDocumentDataResponse_QNAME, ChangeDocumentDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AssignDocumentToUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "assignDocumentToUserResponse")
    public JAXBElement<AssignDocumentToUserResponse> createAssignDocumentToUserResponse(AssignDocumentToUserResponse value) {
        return new JAXBElement<AssignDocumentToUserResponse>(_AssignDocumentToUserResponse_QNAME, AssignDocumentToUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCategories }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "getCategories")
    public JAXBElement<GetCategories> createGetCategories(GetCategories value) {
        return new JAXBElement<GetCategories>(_GetCategories_QNAME, GetCategories.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDocumentFromEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "importDocumentFromEmailResponse")
    public JAXBElement<ImportDocumentFromEmailResponse> createImportDocumentFromEmailResponse(ImportDocumentFromEmailResponse value) {
        return new JAXBElement<ImportDocumentFromEmailResponse>(_ImportDocumentFromEmailResponse_QNAME, ImportDocumentFromEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ForwardEmailToOutlookResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "forwardEmailToOutlookResponse")
    public JAXBElement<ForwardEmailToOutlookResponse> createForwardEmailToOutlookResponse(ForwardEmailToOutlookResponse value) {
        return new JAXBElement<ForwardEmailToOutlookResponse>(_ForwardEmailToOutlookResponse_QNAME, ForwardEmailToOutlookResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportDocumentFromPdfResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "importDocumentFromPdfResponse")
    public JAXBElement<ImportDocumentFromPdfResponse> createImportDocumentFromPdfResponse(ImportDocumentFromPdfResponse value) {
        return new JAXBElement<ImportDocumentFromPdfResponse>(_ImportDocumentFromPdfResponse_QNAME, ImportDocumentFromPdfResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOutlookEmails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "getOutlookEmails")
    public JAXBElement<GetOutlookEmails> createGetOutlookEmails(GetOutlookEmails value) {
        return new JAXBElement<GetOutlookEmails>(_GetOutlookEmails_QNAME, GetOutlookEmails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AssignDocumentToUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "assignDocumentToUser")
    public JAXBElement<AssignDocumentToUser> createAssignDocumentToUser(AssignDocumentToUser value) {
        return new JAXBElement<AssignDocumentToUser>(_AssignDocumentToUser_QNAME, AssignDocumentToUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddConceptOneAgencyIndexToDocumentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "addConceptOneAgencyIndexToDocumentResponse")
    public JAXBElement<AddConceptOneAgencyIndexToDocumentResponse> createAddConceptOneAgencyIndexToDocumentResponse(AddConceptOneAgencyIndexToDocumentResponse value) {
        return new JAXBElement<AddConceptOneAgencyIndexToDocumentResponse>(_AddConceptOneAgencyIndexToDocumentResponse_QNAME, AddConceptOneAgencyIndexToDocumentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAssignedDocumentsInDesktopResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://server.ws.ariis.quadnova.com/", name = "getAssignedDocumentsInDesktopResponse")
    public JAXBElement<GetAssignedDocumentsInDesktopResponse> createGetAssignedDocumentsInDesktopResponse(GetAssignedDocumentsInDesktopResponse value) {
        return new JAXBElement<GetAssignedDocumentsInDesktopResponse>(_GetAssignedDocumentsInDesktopResponse_QNAME, GetAssignedDocumentsInDesktopResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "arg1", scope = ImportDocumentFromPdf.class)
    public JAXBElement<byte[]> createImportDocumentFromPdfArg1(byte[] value) {
        return new JAXBElement<byte[]>(_ImportDocumentFromPdfArg1_QNAME, byte[].class, ImportDocumentFromPdf.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "arg1", scope = ImportDocumentFromEmail.class)
    public JAXBElement<byte[]> createImportDocumentFromEmailArg1(byte[] value) {
        return new JAXBElement<byte[]>(_ImportDocumentFromPdfArg1_QNAME, byte[].class, ImportDocumentFromEmail.class, ((byte[]) value));
    }

}
