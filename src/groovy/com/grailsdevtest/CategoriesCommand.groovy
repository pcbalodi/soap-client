package com.grailsdevtest

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.grailsdevtest.client.*
import com.sun.xml.internal.ws.client.BindingProviderProperties
import org.springframework.beans.factory.annotation.Autowired
import grails.util.Holders
import javax.xml.ws.BindingProvider
import org.apache.cxf.message.Message



public class CategoriesCommand extends HystrixCommand<List<Category>> {

  def config = Holders.grailsApplication.config

  def soapClient = Holders.grailsApplication.mainContext.getBean('soapClient')

  public CategoriesCommand() {
    super(HystrixCommandGroupKey.Factory.asKey("CategoriesGroup"));
  }


  /**
   * Entry point for circuit breaker,
   * this is called when execute() is called on the object.
   */
  @Override
  protected List<Category> run() {
    this.getCategoriesWithRandomDelay()
  }


  /**
   * Method which is called if circuit breaks,
   * we are returning empty list since categories are not received from the web service.
   */
  @Override
  protected List<Category> getFallback(){
    []
  }

  /**
   * Makes web service call to fetch categories. calls may be randomly delayed.
   */
  List<Category> getCategoriesWithRandomDelay() {
    List<Category> categories = getCategories()
    Random random = new Random()
    if(random.nextBoolean()){
      Thread.sleep(getRandomDelayTime())
    }
    categories
  }

  /**
   * Method which actually makes the web-service call.
   */
  List<Category> getCategories(){
    def port=soapClient.getRequestWebServiceEndpointTestImplPort()
    BindingProvider bp = (BindingProvider)port;
    bp.getRequestContext().put(Message.ENDPOINT_ADDRESS,config.ws.serviceEndPoint);
    bp.requestContext.put(BindingProviderProperties.CONNECT_TIMEOUT, config.ws.connectionTimeOut as int);
    bp.requestContext.put(BindingProviderProperties.REQUEST_TIMEOUT, config.ws.readTimeOut as int);
    port.getCategories(config.ws.key,config.ws.username);
  }

  /**
   * Get Some random value of Delay between min and max timeouts , which are specified in Config.
   */
  private long getRandomDelayTime(){
    int min = config.ws.delay.min as int
    int max = config.ws.delay.max as int
    (max + (long)(Math.random() * ((max - min) + 1)) * 1000)
  }


}